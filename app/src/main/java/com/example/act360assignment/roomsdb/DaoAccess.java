package com.example.act360assignment.roomsdb;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.act360assignment.PostModel;

import java.util.List;

@Dao
public interface DaoAccess {

    @Insert
    void insertTask(List<PostModel> postModel);


    @Query("SELECT * FROM PostModel ")
    List<PostModel> fetchAllTasks();

    @Query("DELETE FROM PostModel ")
    void delete();
}