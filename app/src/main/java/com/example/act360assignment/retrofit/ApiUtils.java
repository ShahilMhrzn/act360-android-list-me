package com.example.act360assignment.retrofit;

import android.content.Context;

public class ApiUtils {

    public final static String BASE_URL = "https://jsonplaceholder.typicode.com/";

    public static SOService getSOService(Context mContext) {
        return RetrofitClient.getClient(BASE_URL).create(SOService.class);
    }
}