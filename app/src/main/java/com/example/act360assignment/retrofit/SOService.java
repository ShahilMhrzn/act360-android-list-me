package com.example.act360assignment.retrofit;

import com.example.act360assignment.PostModel;
import com.example.act360assignment.postdetails.PostDetailModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface SOService {

    @GET("posts/")
    Call<List<PostModel>> getList();

    @GET("posts/{id}/comments")
    Call<List<PostDetailModel>> getDetails(
            @Path("id") int user
    );
}