package com.example.act360assignment.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Movie;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.act360assignment.PostModel;
import com.example.act360assignment.R;
import com.example.act360assignment.postdetails.PostDetailActivity;

import java.util.List;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.MyViewHolder> {

    private List<PostModel> postModelList;
    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title,id;
        LinearLayout row;

        public MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.title);
            id = view.findViewById(R.id.id);
            row = view.findViewById(R.id.row);
        }
    }


    public PostAdapter(List<PostModel> postModelList1, Context mContext) {
        this.postModelList = postModelList1;
        context = mContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_postlist_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final PostModel post = postModelList.get(position);
        holder.id.setText(post.getTitle());
        holder.title.setText(post.getBody());
        holder.row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PostDetailActivity.class);
                intent.putExtra("id",post.getId() + "");
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return postModelList.size();
    }
}
