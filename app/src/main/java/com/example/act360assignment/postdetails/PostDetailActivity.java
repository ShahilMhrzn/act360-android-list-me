package com.example.act360assignment.postdetails;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.example.act360assignment.MainActivity;
import com.example.act360assignment.PostModel;
import com.example.act360assignment.R;
import com.example.act360assignment.adapter.PostAdapter;
import com.example.act360assignment.retrofit.ApiUtils;
import com.example.act360assignment.retrofit.SOService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostDetailActivity extends AppCompatActivity {
    private SOService mService;
    private List<PostDetailModel> modelList = new ArrayList<>();
    private RecyclerView recyclerView;
    int id =1;
    private PostDetailAdapter mAdapter;
    PostDetailModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_postdetail);

        mService = ApiUtils.getSOService(this);

        recyclerView =findViewById(R.id.comments);
        mAdapter = new PostDetailAdapter(modelList,this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        // Get the transferred data from source activity.
        Intent intent = getIntent();
        String sid =  intent.getStringExtra("id");

        id = Integer.parseInt(sid);

        mService.getDetails(id).enqueue(new Callback<List<PostDetailModel>>() {
            @Override
            public void onResponse(Call<List<PostDetailModel>> call, Response<List<PostDetailModel>> response) {
                if (response.isSuccessful()) {

                    for (int i = 0 ;i < response.body().size();i++){
                        if (response.body().get(i).getPostId() == id){
                            model = new PostDetailModel(response.body().get(i).getPostId(),response.body().get(i).getId(),response.body().get(i).getName(),response.body().get(i).getEmail(),response.body().get(i).getBody());
                            modelList.add(model);
                            Toast.makeText(PostDetailActivity.this, ""+i, Toast.LENGTH_SHORT).show();
                        }
                    }

                    mAdapter.notifyDataSetChanged();
                } else {
                    int statusCode = response.code();
                    Toast.makeText(PostDetailActivity.this, "a", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<PostDetailModel>> call, Throwable t) {
//                Toast.makeText(PostDetailActivity.this, "fail", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
