package com.example.act360assignment.postdetails;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.act360assignment.PostModel;
import com.example.act360assignment.R;

import java.util.List;

public class PostDetailAdapter extends RecyclerView.Adapter<PostDetailAdapter.MyViewHolder> {

    private List<PostDetailModel> postModelList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        public MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.id);
        }
    }


    public PostDetailAdapter(List<PostDetailModel> postModelList1, Context context) {
        this.postModelList = postModelList1;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_postlist_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        PostDetailModel post = postModelList.get(position);
        holder.title.setText(post.getEmail());
    }

    @Override
    public int getItemCount() {
        return postModelList.size();
    }
}
