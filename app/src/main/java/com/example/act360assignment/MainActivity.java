package com.example.act360assignment;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.graphics.Movie;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.act360assignment.adapter.PostAdapter;
import com.example.act360assignment.retrofit.ApiUtils;
import com.example.act360assignment.retrofit.SOService;
import com.example.act360assignment.roomsdb.PostDatabase;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private SOService mService;
    PostModel movie;
    private List<PostModel> modelList = new ArrayList<>();
    private RecyclerView recyclerView;
    private PostAdapter mAdapter;
    PostModel post;

    private String DB_NAME = "db_task";
    private PostDatabase postDatabase;
    private Button delete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mService = ApiUtils.getSOService(this);

        delete = findViewById(R.id.delete);
        postDatabase = Room.databaseBuilder(this,PostDatabase.class,DB_NAME).allowMainThreadQueries().build();

        recyclerView = (RecyclerView) findViewById(R.id.post_list);
        mAdapter = new PostAdapter(modelList,this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);


        prepareMovieData();

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postDatabase.daoAccess().delete();
                finish();
                Intent intent = new Intent(MainActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }
    private void prepareMovieData() {
        try {
            Toast.makeText(this, ""+postDatabase.daoAccess().fetchAllTasks().size(), Toast.LENGTH_SHORT).show();
            modelList.addAll(postDatabase.daoAccess().fetchAllTasks());
        }catch (Exception e){
            Toast.makeText(this, "aa", Toast.LENGTH_SHORT).show();
        }
        mService.getList().enqueue(new Callback<List<PostModel>>() {
            @Override
            public void onResponse(Call<List<PostModel>> call, Response<List<PostModel>> response) {
                if (response.isSuccessful()) {

                    modelList.addAll(response.body());
                    try{
                        postDatabase.daoAccess().insertTask(modelList);
                    }catch (Exception e){
                        Toast.makeText(MainActivity.this, "aaa"+e, Toast.LENGTH_SHORT).show();
                    }
                    mAdapter.notifyDataSetChanged();
                } else {
                    int statusCode = response.code();
                    Toast.makeText(MainActivity.this, "a", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<PostModel>> call, Throwable t) {
                Toast.makeText(MainActivity.this, "fail", Toast.LENGTH_SHORT).show();
            }
        });


        mAdapter.notifyDataSetChanged();

    }
}
